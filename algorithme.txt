## importer un fichier de donnée

APR <- 3L
NOV <- 10L

start <- as.Date(start)
end <- as.Date(end)

start.mon <- as.POSIXlt(start)$mon
end.mon <- as.POSIXlt(end)$mon

dates <- seq(start + 1, by = "+1 month",
             length.out = end.mon - start.mon + 1L) - 1

imporData <- function(start, end, path = getwd())

## selection de données d'un tableau entre deux dates

date <- as.POSIXlt(c((start)$mon, (end)$mon))

## fusionner deux tableaux de données

m <- matrix(5:3, nrow = 5)

n <- matrix(4:3, nrow = 4)

l <- rbin(m,n)

m <- matrix(5:3, ncol = 3)
n <- matrix(5:4, ncol = 4)

l <- cbin(m,n)




             