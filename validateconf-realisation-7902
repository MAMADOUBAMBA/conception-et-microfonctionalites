## Copyright (C) 2023 Vincent Goulet
##
## Ce fichier fait partie du cours
## IFT-4902/7902 Programmation avec R pour l'analyse de données
##
## Cette création est mise à disposition sous licence
## Attribution-Partage dans les mêmes conditions 4.0 International de
## Creative Commons. https://creativecommons.org/licenses/by-sa/4.0/

__global
  repository
    check_repos: true
    project_name: h2024-15917
    pattern_name: ^[0-9]{9}_[Tt]ravail[Ll]ong$
  defaults
    check_exists_laden: true
    check_contents: true
    check_encoding: true

00-commits
  description: "Nombre d'archives dans le dépôt"
  check_exists_laden: false
  check_contents_module: validate-repos-commits 10
  check_encoding: false

01-importStations.R

02-importData.R

03-getStations.R

04-summary.duration.R

05-summary.rentals.R

06-revenues.R

07-filename.R

08-tariff_2020.R

09-tariff_2023.R

10-calcul.R

42-bixi-2020
  recursive: true
  description: "Données pour calcul.R"
  check_exists_laden: true
  check_contents: false
  check_encoding: false
  00-Stations_2020u.csv
    check_contents: false
    check_encoding: false
  06-OD_2020-06u.csv
    check_contents: false
    check_encoding: false
  07-OD_2020-07u.csv
    check_contents: false
    check_encoding: false
  08-OD_2020-08u.csv
    check_contents: false
    check_encoding: false
  09-OD_2020-09u.csv
    check_contents: false
    check_encoding: false
  10-OD_2020-10u.csv
    check_contents: false
    check_encoding: false

99-analyse-critique-realisation.txt
  check_contents: false

11-getRentals.R

12-tests-getRentals.R
