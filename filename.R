Définir la fonction.
source("filename.R")

## Données de test.
DATE_SINGLE <- c("2042-07-22")
DATE_VECTOR <- c("2042-08-10", "2042-09-24", "2042-10-11")
PATH <- "data"

## Valeurs cibles
EXPECTED_SINGLE_RESULT <- file.path(getwd(), "OD_2042-07u.csv")
EXPECTED_VECTOR_RESULT <- file.path(getwd(), c("OD_2042-08u.csv",
                                               "OD_2042-09u.csv",
                                               "OD_2042-10u.csv"))
EXPECTED_PATH_RESULT <- file.path("data", "OD_2042-07u.csv")

## Tests sur la création de noms de fichiers sans chemin d'accès.
stopifnot(identical(EXPECTED_SINGLE_RESULT,
                    try(filename(date = DATE_SINGLE))))
stopifnot(identical(EXPECTED_VECTOR_RESULT,
                    try(filename(date = DATE_VECTOR))))

## Test sur la création d'un nom de fichier avec chemin
## d'accès.
stopifnot(identical(EXPECTED_PATH_RESULT,
                    try(filename(path = PATH, date = DATE_SINGLE))))

###
### Tests unitaires de la fonction 'tariff_2020'.
###

## Définir la fonction.
source("tariff_2020.R")

## Données de test. Nous prenons aussi soin de tester les cas limites:
##
## - une durée nulle;
## - une durée de moins de 30 minutes;
## - une durée de 30 minutes exactement;
## - une durée entre 30 et 31 minutes;
## - une durée de 31 minutes exactement;
## - une durée entre 31 et 45 minutes;
## - une durée de 45 minutes exactement;
## - une durée entre 45 et 46 minutes;
## - une durée de 46 minutes exactement;
## - une durée entre 46 et 60 minutes
##   (une tranche de 15 minutes additionnelles);
## - une durée entre 60 et 75 minutes
##   (deux tranches de 15 minutes additionnelles).
ZERO_DURATION <- 0
SINGLE_DURATION <- 3 * 60
VECTOR_DURATIONS <- c(23, 30, 30, 31, 33, 45, 45, 46, 53, 73) * 60 +
    c( 0,  0,  2,  0,  0,  0,  1,  0,  0,  0)

## Valeurs cibles.
EXPECTED_ZERO_RESULT <- 2.99
EXPECTED_SINGLE_RESULT <- 2.99
EXPECTED_VECTOR_RESULTS <- 2.99 +
    c(0, 0, 1.80 + c(0, 0, 0, 0, 3.00 * c(1, 1, 1, 2)))

## Test avec une durée de 0 seconde.
stopifnot(all.equal(EXPECTED_ZERO_RESULT,
                    try(tariff_2020(ZERO_DURATION))))

## Test avec une seule durée.
stopifnot(all.equal(EXPECTED_SINGLE_RESULT,
                    try(tariff_2020(SINGLE_DURATION))))

## Test avec un vecteur de durées.
stopifnot(all.equal(EXPECTED_VECTOR_RESULTS,
                    try(tariff_2020(VECTOR_DURATIONS))))
